/*
 * @Project Name: TODO
 * @File Name: TimeConverterImpl
 * @Package Name: com.paic.arch.interviews
 * @Date: 2018/3/3 9:19
 * @Creator:
 * @line------------------------------
 * @修改人:
 * @修改时间:
 * @修改内容:
 */

package com.paic.arch.interviews;

import java.util.function.BiFunction;
import java.util.regex.Pattern;

/**
 * @author liuliyang
 * @date 2018年3月3日上午10:28:02
 */
public class TimeConverterImpl implements TimeConverter {

	// regex expression for checking time format
	private static final String REG_TIME = "(([0,1]\\d|2[0-3]):([0-5]\\d):([0-5]\\d))";
	// the single color light function
	private static final BiFunction<Integer, Integer, String> SINGLE_FUNC = (index, row) -> {
		if (row == 5) {
			return LightColorEnum.Yellow.getColor();
		} else {
			return LightColorEnum.Red.getColor();
		}
	};
	//multiple colors light function
	private static final BiFunction<Integer, Integer, String> MIX_FUNC = (index, row) -> {
		if (index % 3 == 0) {
			return LightColorEnum.Red.getColor();
		}
		return LightColorEnum.Yellow.getColor();
	};

	/**
	 * 获取灯颜色的字符串表示
	 * @param func : 根据灯所处的行数,以及灯的位置数,确定灯的颜色
	 * @param row : 灯所处的行数,从1开始计数,灯从上往下计数
	 * @param num : 需要亮灯的个数
	 * @param total : 该行灯的总数
	 * @return 该行所有灯的颜色标识
	 */
	private StringBuilder getColor(BiFunction<Integer, Integer, String> func, int row, int num, int total) {
		StringBuilder builder = new StringBuilder();
		int maxLightOnIndex = num + 1;
		for (int i = 1; i < maxLightOnIndex; i++) {
			builder.append(func.apply(i, row));
		}
		for (int j = maxLightOnIndex; j < total + 1; j++) {
			builder.append(LightColorEnum.Off.getColor());
		}
		return builder;
	}

	/**
	 * @param hours 小时数
	 * @param minutes 分钟数
	 * @param seconds 秒数
	 * @return theory clock 字符串表示结果
	 */
	private String doConverTime(int hours, int minutes, int seconds) {
		String[] nights = new String[5];
		int hour1 = (int) (hours / 5);
		int hour2 = hours % 5;
		int minute1 = (int) minutes / 5;
		int minute2 = minutes % 5;
		boolean isLight = seconds % 4 == 0;
		//为了便于理解,使用字符串分开表示每行灯的展示情况
		String secondStr = (isLight ? LightColorEnum.Yellow.getColor() : LightColorEnum.Off.getColor());
		StringBuilder hour1Str = getColor(SINGLE_FUNC, 2, hour1, 4);
		StringBuilder hour2Str = getColor(SINGLE_FUNC, 3, hour2, 4);
		StringBuilder min1Str = getColor(MIX_FUNC, 4, minute1, 11);
		StringBuilder min2Str = getColor(SINGLE_FUNC, 5, minute2, 4);
		StringBuilder res = new StringBuilder();
		res.append(secondStr).append("\r\n").append(hour1Str).append("\r\n").append(hour2Str).append("\r\n")
				.append(min1Str).append("\r\n").append(min2Str);
		return res.toString();
	}

	@Override
	public String convertTime(String aTime) {
		//check the time format . In order to improve checking performance, check 24:00:00 by 'equals' method.
		if (aTime.equals("24:00:00") || Pattern.matches(REG_TIME, aTime)) {
			String[] hms = aTime.split(":");
			return doConverTime(Integer.valueOf(hms[0]), Integer.valueOf(hms[1]),
					hms.length == 3 ? Integer.valueOf(hms[2]) : 0);
		} else {
			throw new RuntimeException("the time [" + aTime + "] format is wrong, please check!");
		}
	}
}
