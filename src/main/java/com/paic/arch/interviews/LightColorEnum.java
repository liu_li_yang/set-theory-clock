
package com.paic.arch.interviews;

/**
 * @author liuliyang
 * @date 2018年3月3日上午10:28:02
 */
public enum LightColorEnum {
	Red("R"), Yellow("Y"), Off("O");

	private String color;

	LightColorEnum(String color) {
		this.color = color;
	}

	public String getColor() {
		return color;
	}
}
